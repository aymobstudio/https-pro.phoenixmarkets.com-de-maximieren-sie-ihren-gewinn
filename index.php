<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Phoenix Markets - Maximieren Sie Ihren Gewinn mit Netflix CFDs</title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" >
        <link rel="stylesheet" href="assets/css/style.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css" />
 
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  
       <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M7LW664');</script>
<!-- End Google Tag Manager -->

</head>
    <body>
<?php 
    $date = new DateTime();
    $initdate = $date->getTimestamp();
?>

      <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7LW664" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    

    <!-- Server Api Response Error -->
    <?php if (isset($_GET['error'])) {
    $error = $_GET['error'];
  ?> <script type="text/javascript">
    $(document).ready(function() {
        $("#server_error_msg").show();
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 4000);
    })
    </script>
    <?php } else {
    $error = ""; ?>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#server_error_msg").hide();
    })
    </script>
    <?php } ?>

        <!-- Desktop Hero Section -->
        <section class="hero-section text-white">
        
            <div class="container">
                
                <div class="row">
                <!-- <div class="main-logo text-center">
                    <img src="assets/img/form_title_logo.png" alt="logo" class="logo rounded mx-auto d-block ">
                </div>     -->
                    <div class="col-md-8 mt-auto cta col-12">
                        <h4>Advertorial</h4>
                        <h2>Maximieren Sie Ihren<br> Gewinn mit Netflix CFDs</h2>
                        <p class="d-none d-md-block">Die beste Möglichkeit, ein zweites Einkommen durch Handel zu schaffen</p>
                        <div class="hero-btn d-md-none pb-3">
                        <a href="#" class="btn btn-danger">Berechnen Sie Ihr Einkommen</a>
                        </div>
                    </div>
                    <div class="col-md-4 p-3 form-col mb-3 col-12">
                        <img src="assets/img/form_title_logo.png" alt="logo" class="logo rounded mx-auto d-block ">
                          <!-- Api Response Error -->
                          <div class="alert alert-danger alert-dismissible" role="alert"
                                    id="server_error_msg">
                                    <strong>Error!</strong> <?php echo $error; ?>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                </div>
                                
                        <form class="mt-3 p-3" action="form.php" method="POST" onsubmit="return Validation()">
                            
                                <!-- Click id -->
                                <?php if (isset($_GET['clickid']) && ($_GET['clickid'] != "")) { ?>
                                <input type="hidden" value="<?php echo $_GET['clickid']; ?>" name="clickid" />
                                <?php  } ?>
                                <input type="hidden" id="initdate" name="initdate"  value="<?= $initdate; ?>">

                            <div class="form-group pb-3">
                                <input type="text" name="fname" id="fname" class="form-control" required="" placeholder="Voller Name*" onkeyup="checkName();" autocomplete="off">
                                <span id="fname-message" class="errors-message" ></span>
                            </div>
                            <div class="form-group pb-3">
                                <input type="email" name="email" id="email" class="form-control" required="" placeholder="E-mail*" autocomplete="off">
                                <span id="email-message" class="errors-message" ></span>
                            </div>

                            <div class="form-group pb-3">
                                <input type="password" name="password" id="password" class="form-control" required="" placeholder="Password*" onkeyup="check_password();" autocomplete="off">
                                <span id="password-message" class="errors-message" ></span>
                            </div>

                            <div class="form-group pb-3">
                                <input type="text" name="phone" id="phone" required="" class="form-control" placeholder="" style="padding-left: 50px !important;" autocomplete="off">
                                <span id="phone-message" class="errors-message" ></span>
                            </div>
                            <br>
                            <div class="form-group pb-3">
                                <div class="row">
                                    <div class="col-md-1">
                                        <input type="checkbox" name="check" checked="checked" id="check">
                                    </div>
                                    <div class="col-md-11">
                                        <label for="check" id="checklabel" class="text-dark">Ich habe die <a href="política-de-privacidad.html">Datenschutzerklärung</a>, die <a href="términos-y-condiciones.html">Nutzungsbedingungen</a> und den Erhalt von Werbematerial gelesen und akzeptiere diese.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <button class="btn btn-danger w-100" type="submit">Beginnen Sie!</button>
                            </div>
                            <div class="form-group">
                                <p class="text-dark">Sie müssen mindestens 18 Jahre alt sein, um investieren zu können. Mindesteinzahlung von 250 €</p>
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <img src="assets/img/form_logo1.png" alt="Visa">
                                        </div>
                                        <div class="col">
                                            <img src="assets/img/form_logo2.png" alt="mastercard">
                                        </div>
                                        <div class="col">
                                            <img src="assets/img/form_logo3.png" alt="Visa">
                                        </div>
                                        <div class="col">
                                            <img src="assets/img/form_logo4.png" alt="paypal">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <section class="overlapping">
            <div class="container">
                <div class="row">
                    <div class="bg-white col-md-5 offset-md-4 logo-overlap p-3">
                        <img src="assets/img/hero_logos.png" alt="hero logos" class="rounded mx-auto d-block">
                    </div>
                </div>
            </div>
        </section>


        
        <!-- Range Slider -->
        <section class="range-section p-3">
            <div class="container">
                <div class="row shadow">
                    <div class="col">
                        <div class="range-slider">
                           <div class="row">
                               <div class="col-md-8 mt-auto pb-3">
                                <div class="title">
                                    Berechnen Sie Ihr Verdienstpotenzial
                                </div>
                                <div class="subTitle pb-3">
                                    Mindestanlage von 250 €
                                </div>
                                <input class="range-slider__range" type="range" value="250" min="0" max="5000" step="50">
                            <p class="rangep">
                            €
                                <i class="priceFixed">250</i>
                            </p>   
                            </div>

                               <div class="col-md-4 text-center">
                                   <div class="price pb-3">
                                    €
                                    <span class="range-slider__value">250</span>
                                   </div>
                                
                                <a href="#" class="btn btn-danger">Berechnen Sie den potenziellen Nutzen</a>
                               </div>
                           </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Signin Up Steps -->

        <section class="signing-up">
            <div class="container">
                <div class="row pb-5">
                    <div class="col">
                        <h2 class="title">Die Anmeldung ist einfach. Sie können darauf zugreifen:</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="blurb">
                            <img src="assets/img/bag.svg" alt="Platform Sicher und geregelt">
                            <p class="title">Plattform <br> sicher und geregelt</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="blurb">
                            <img src="assets/img/donation.svg" alt="Platform Sicher und geregelt">
                            <p class="title">Einnahmen auf Ihrem Konto<br> in 24 Stunden</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="blurb">
                            <img src="assets/img/graph.svg" alt="Platform Sicher und geregelt">
                            <p class="title">Einfache<br> Lernwerkzeuge</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="blurb">
                            <img src="assets/img/chat.svg" alt="Platform Sicher und geregelt">
                            <p class="title">24 Stunden Support und<br> kontinuierliche Unterstützung</p>
                        </div>
                    </div>
                </div>


                <div class="row pt-5">
                    <div class="col-md-4 offset-md-4 text-center">
                        <a href="#" class="btn btn-danger w-100">Jetzt beraten lassen!</a>
                    </div>
                </div>
            </div>
        </section>


        <!-- benefits section -->
<section class="benefits">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-md-6 ">
                <div class="cta">
                    <h2 class="title pb-3">Fangen Sie jetzt an, Vorteile zu haben</h2>
                    <p class="description pt-3">
                        Netflix hat bisher einen unglaublichen Weg eingeschlagen, aber die Dinge laufen gerade erst warm. Viele Investoren haben es geschafft, ihre Gewinne in den letzten Jahren zu verdoppeln und sogar zu verdreifachen. Bei einem Umsatz von BILLIONEN sagen Experten noch mehr Wachstum innerhalb eines Jahres voraus, da immer wieder neue Produkte auf den Markt kommen. Es ist noch nicht zu spät, um einzusteigen:
                        <br>
                        <br>
                        Netflix gehört nach wie vor zu den Top-Unternehmen für Investitionen.
                    </p>
                    <a href="#" class="btn btn-danger">Jetzt investieren</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="assets/img/mobilePhone.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>



<!-- Tested and verified Section -->

<section class="testing-section text-center p-5">
    <div class="container">
        <div class="row pb-5">
            <div class="col">
                <h2 class="heading">Getestet und verifiziert</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="blurb">
                    <img src="assets/img/bag.svg" alt="Platform Sicher und geregelt">
                    <h2 class="title">Garantie</h2>
                    <p class="subTitle">Voll autorisiert, Zertifikat <br>und registriert</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="blurb">
                    <img src="assets/img/donation.svg" alt="Platform Sicher und geregelt">
                    <h2 class="title">Sicherheit</h2>
                    <p class="subTitle">Unsere Protokolle<br>Sicherheitsgarantie dass deine Fonds Sein vollständig geschützt</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="blurb">
                    <img src="assets/img/graph.svg" alt="Platform Sicher und geregelt">
                    <h2 class="title">Datenschutz</h2>
                    <p class="subTitle">Ihre Daten sind sicher und vertraulichen</p>
                </div>
            </div>


        </div>


        <div class="row pt-5">
            <div class="col-md-4 offset-md-4 text-center">
                <a href="#" class="btn btn-danger px-5">Verdienen beginnen</a>
            </div>
        </div>
    </div>
</section>


<!-- Footer -->

<footer>
    <div class="container">
        <div class="row pb-3">
            <div class="col">
                <div class="footer-links">
                    <a href="https://www.phoenixmarkets.com/doc/regulation/policies/terms_and_conditions.pdf">Geschäftsbedingungen</a>
                    <a href="https://www.phoenixmarkets.com/doc/regulation/policies/privacy_policy.pdf">Datenschutzerklärung</a>
                    <a href="https://www.phoenixmarkets.com/doc/regulation/policies/cookies_policy.pdf">Cookie-Richtlinie</a>
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
            <p>WARNUNG INVESTITIONEN MIT HOHEM RISIKO: CFDs sind komplexe Instrumente und bergen ein hohes Risiko, aufgrund der Hebelwirkung Geld zu verlieren. 80% der Privatanlegerkonten verlieren beim CFD-Handel bei diesem Anbieter Geld. Sie sollten sich überlegen, ob Sie verstehen, wie CFDs funktionieren und ob Sie es sich leisten können, das hohe Risiko einzugehen, Ihr Geld zu verlieren. Bitte klicken Sie hier, um die vollständige Risikowarnung zu lesen. WGM Service Ltd ist ein Finanzdienstleistungsunternehmen, das von der Cyprus Securities Exchange Commission (CySEC) unter der Lizenz-Nr. 203/13. WGM Services Ltd hat seinen Sitz in 11, Vizantiou, 4th Floor, Strovolos 2064, Nikosia, Zypern. Die auf dieser Website enthaltenen Informationen und Dienste richten sich nicht an Einwohner der Vereinigten Staaten von Amerika und Kanadas.</p>
            </div>
        </div>
    </div>
</footer>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="assets/js/myScript.js"></script>
    </body>



    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <script>
    const phoneInputField = document.querySelector("#phone");
    const phoneInput = window.intlTelInput(phoneInputField, {
        initialCountry: "auto",
        geoIpLookup: function(success) {
            // Get your api-key at https://ipdata.co/
            fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
                .then(function(response) {
                    if (!response.ok) return success("");
                    return response.json();
                })
                .then(function(ipdata) {
                    success(ipdata.country_code);
                });
        },
        utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
    });

    $('#commence-btn').click(function() {
        var code = phoneInput.getNumber();
        $('#phone').val(code);
    });

    // phone number validation
    $('#phone').first().keyup(function() {
        var phone = this.value;
        if (phone.length < 5) {
            document.getElementById("phone-message").innerHTML = "This field is required.";
        } else {
            document.getElementById("phone-message").innerHTML = " ";
            return true;
        }
    });


    //   validation function
    function Validation() {
        var pw = document.getElementById("password").value;
        if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
            document.getElementById("password-message").innerHTML =
                "Password length must be at least 8 characters. Password must contain at least one letter, one uppercase and one number";
            return false;
        }
        if (pw.length > 15) {
            document.getElementById("password-message").innerHTML =
                "Password length must not exceed 15 characters.";
            return false;
        }

        var regExp = new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})");
        var fname = document.getElementById("fname").value;
        if ((fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname)) {
            document.getElementById("fname-message").innerHTML = "The name must be followed by First name, Space then Last name.";
            return false;
        }

        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var emails = document.getElementById("email").value;
        if ((emails.length < 2) || (!emailReg.test(emails))) {
            document.getElementById("email-message").innerHTML =
                "This field is required, please enter a valid email address";
            return false;
        }

        var phone = document.getElementById("phone").value;
        if (phone.length < 5) {
            document.getElementById("phone-message").innerHTML = "This field is required.";
            return false;
        }
    }
    </script>

    <script>
    var checkName = function() {
        var regExp = new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})");
        var fname = document.getElementById("fname").value;
        if ((fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ")
                .length > 2)) {
            document.getElementById("fname-message").innerHTML =
                "The name must be followed by First name, Space then Last name.";
            return false;
        }
        document.getElementById("fname-message").innerHTML = " ";
        return true;
    }
    </script>


    <script>
    $('#email').first().keyup(function() {
        var $email = this.value;
        validateEmail($email);
    });

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if ((email.length < 2) || (!emailReg.test(email))) {
            document.getElementById("email-message").innerHTML = "This field is required.Please enter a valid email";
        } else {
            document.getElementById("email-message").innerHTML = " ";
            return true;
        }
    }
    </script>

    <script>
    var check_password = function() {
        var pw = document.getElementById("password").value;
        if (pw.length < 8) {
            document.getElementById("password-message").innerHTML =
                "Password length must be at least 8 characters.";
            return false;
        }
        if ((pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
            document.getElementById("password-message").innerHTML =
                "Password must contain at least one letter, one uppercase and one number";
            return false;
        }
        document.getElementById("password-message").innerHTML = " ";
        return true;
    }
    </script>
</html>